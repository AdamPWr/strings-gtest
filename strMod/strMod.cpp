#include"strMod.h"

string strMod::reverse(string str)
{
    size_t size = str.size();
    char buff;

    for(auto i = 0;i<size/2;++i)
    {
        buff = str[size - i-1];
        str[size-1-i] = str[i];
        str[i] = buff;
    }

    return str;
}