#include<gtest/gtest.h>
#include"../strMod/strMod.h"
#include<memory>


using  namespace std;

typedef  std::pair<string,string> val;


struct strModTest : public ::testing::TestWithParam<val>
{
    unique_ptr<strMod> m_strMod = make_unique<strMod>();

    void SetUp()
    {
        
    }
};

INSTANTIATE_TEST_CASE_P(default,strModTest,testing::Values(
    val("kot","tok"),
    val("",""),
    val("kamil","limak"),
    val(" Adam","madA "),
    val("  2spacje3spacje   ","   ejcaps3ejcaps2  ")
    ));


TEST_P(strModTest,reverse_string)
{
    EXPECT_EQ(m_strMod->reverse(GetParam().first),GetParam().second);
}